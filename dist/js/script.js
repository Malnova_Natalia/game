const data = {
  "rating": [{
    "id": "123",
    "name": "Владимир",
    "lastName": "Ларионов",
    "img": "male.png",
    "points": "463"
  }, {
    "id": "9",
    "name": "Владимир",
    "lastName": "Сергеев",
    "img": "male.png",
    "points": "521"
  }, {
    "id": "231",
    "name": "Вениамин",
    "lastName": "Васильев",
    "img": "male.png",
    "points": "865"
  }, {
    "id": "321",
    "name": "Мария",
    "lastName": "Логинова",
    "img": "male.png",
    "points": "865"
  }, {
    "id": "492",
    "name": "Борис",
    "lastName": "Казанцев",
    "img": "male.png",
    "points": "784"
  }, {
    "id": "452",
    "name": "Полина",
    "lastName": "Калинина",
    "img": "male.png",
    "points": "225"
  }, {
    "id": "796",
    "name": "Даниил",
    "lastName": "Воробьёв",
    "img": "male.png",
    "points": "642"
  }, {
    "id": "4",
    "name": "Эрик",
    "lastName": "Аксёнов",
    "img": "male.png",
    "points": "150"
  }, {
    "id": "1155",
    "name": "Иван",
    "lastName": "Иванов",
    "img": "male.png",
    "points": "100"
  }, {
    "id": "12145",
    "name": "Артем",
    "lastName": "Алексеев",
    "img": "male.png",
    "points": "1000"
  }],
  "friends": [{
    "id": "9",
    "name": "Владимир",
    "lastName": "Сергеев",
    "img": "male.png"
  }, {
    "id": "4",
    "name": "Эрик",
    "lastName": "Аксёнов",
    "img": "male.png"
  }, {
    "id": "15411",
    "name": "Ирина",
    "lastName": "Чеснокова",
    "img": "male.png"
  }, {
    "id": "15564",
    "name": "Дарина",
    "lastName": "Боброва",
    "img": "male.png"
  }]
};

const play = () => {
  const cicle_start = document.querySelector('.cicle_start');
  const point = document.querySelectorAll('.cicle');
  const man = document.querySelector('svg .game_man');
  let position_x = cicle_start.x.animVal.value;
  let position_y = cicle_start.y.animVal.value;
  man.setAttribute('x', position_x + 5);
  man.setAttribute('y', position_y - 51);
  man.setAttribute('data-position', 'point-1');
  const btn = document.querySelector('.navigate_start');
  btn.addEventListener('click', () => {
    const pos = man.getAttribute('data-position').slice(6);
    if (pos == point.length) return;
    let position_x = point[pos].x.animVal.value;
    let position_y = point[pos].y.animVal.value;
    const new_pos = +pos + 1;
    if (new_pos > point.length) return;
    man.setAttribute('data-position', 'point-' + new_pos);
    man.setAttribute('x', position_x + 4);
    man.setAttribute('y', position_y - 59);
  });
};

const slider = () => {
  const next = document.querySelector('.nav_next');
  const prev = document.querySelector('.nav_prev');
  const slider = document.querySelector('.slider');
  const slider_wrap = document.querySelector('.slider_wrap');
  let margin_left = 0;
  next.addEventListener('click', () => {
    margin_left -= 60;
    console.log('slider = ', slider.clientWidth);
    console.log('slider_wrap = ', slider_wrap.clientWidth);

    if (slider.clientWidth - margin_left + 12 > slider_wrap.clientWidth) {
      margin_left += 60;
      return;
    }

    slider_wrap.style.marginLeft = margin_left + 'px';
    console.log('next', slider.clientWidth - margin_left + 12);
  });
  prev.addEventListener('click', () => {
    if (margin_left == 0) return;
    margin_left += 60;
    slider_wrap.style.marginLeft = margin_left + 'px';
    console.log('next', margin_left);
  });
};

const rating = () => {
  data.rating.sort((a, b) => parseFloat(b.points) - parseFloat(a.points));
  let div = document.createElement('div');
  div.classList.add("raiting");
  let content = '';

  for (let i = 0; i < data.rating.length; i++) {
    content += `
        <div class="raiting_item">
            <div class="place">${i + 1}
                <div class="rating_photo">
                    <img src="img/${data.rating[i].img}" alt="">
                </div>
            </div>`;

    if (data.friends.filter(x => x.id === data.rating[i].id).length != 0) {
      content += `<p class="fio fio_active">${data.rating[i].name} ${data.rating[i].lastName}</p>`;
    } else {
      content += `<p class="fio">${data.rating[i].name} ${data.rating[i].lastName}</p>`;
    }

    content += `<p class="expert">${data.rating[i].points}</p>
                </div>
            `;
  }

  div.innerHTML = content;
  document.querySelector('.modal_content').appendChild(div);
};

const show = () => {
  const btn = document.querySelector('.navigate_raiting');
  const wrap = document.querySelector('.overlay');
  const modal = document.querySelector('.modal');
  const close = wrap.querySelector('.close');
  btn.addEventListener('click', function (e) {
    e.preventDefault();
    wrap.style.zIndex = 999;
    btn.disabled = true;
    setTimeout(() => {
      wrap.classList.add('overlay_open');
      btn.disabled = false;
    }, 100);
    setTimeout(() => {
      modal.classList.add('modal_open');
    }, 600);
  });
  close.addEventListener('click', function (e) {
    clickClose(e);
  });
  wrap.addEventListener('click', function (e) {
    if (e.target == wrap) clickClose(e);
  });

  const clickClose = e => {
    modal.classList.remove('modal_open');
    setTimeout(() => {
      wrap.classList.remove('overlay_open');
    }, 300);
    setTimeout(() => {
      wrap.style.zIndex = -1;
    }, 600);
  };
};

document.addEventListener("DOMContentLoaded", () => {
  play();
  slider();
  rating();
  show();
});