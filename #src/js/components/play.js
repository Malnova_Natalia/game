const play = () => {
    const cicle_start = document.querySelector('.cicle_start')
    const point = document.querySelectorAll('.cicle')

    const man = document.querySelector('svg .game_man')

    let position_x = cicle_start.x.animVal.value
    let position_y = cicle_start.y.animVal.value

    man.setAttribute('x', position_x + 5)
    man.setAttribute('y', position_y - 51)
    
    man.setAttribute('data-position', 'point-1')

    const btn = document.querySelector('.navigate_start')

    btn.addEventListener('click', () => {
      
        const pos =  man.getAttribute('data-position').slice(6)

        if(pos == point.length) return

        let position_x = point[pos].x.animVal.value
        let position_y = point[pos].y.animVal.value
        
        const new_pos = +pos +1
        
        if(new_pos > point.length) return

        man.setAttribute('data-position', 'point-' + new_pos)
        man.setAttribute('x', position_x + 4)
        man.setAttribute('y', position_y - 59)

        
    })
}