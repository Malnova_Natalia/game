const show = () => {

    const btn = document.querySelector('.navigate_raiting')
    const wrap = document.querySelector('.overlay')
    const modal = document.querySelector('.modal')
    const close = wrap.querySelector('.close');

    btn.addEventListener('click', function(e) {

        e.preventDefault();

        wrap.style.zIndex = 999;
        btn.disabled = true;
        setTimeout(() => {
            wrap.classList.add('overlay_open');
            btn.disabled = false;
        }, 100);
        
        setTimeout(() => {
            modal.classList.add('modal_open');
        }, 600);
    });

    close.addEventListener('click', function(e) {
        clickClose(e)
    });


    wrap.addEventListener('click', function(e) {
        if(e.target == wrap) clickClose(e)
        
    });

    const clickClose = (e) => {

        modal.classList.remove('modal_open');
        setTimeout(() => {
            wrap.classList.remove('overlay_open');
        }, 300);
        setTimeout(() => {
            wrap.style.zIndex = -1;
        }, 600);
    }
}
