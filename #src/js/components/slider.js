const slider = () => { 

    const next = document.querySelector('.nav_next')
    const prev = document.querySelector('.nav_prev')

    const slider = document.querySelector('.slider')
    const slider_wrap = document.querySelector('.slider_wrap')
    let margin_left = 0

   
    next.addEventListener('click', () => {
    
        margin_left -= 60

        console.log('slider = ', slider.clientWidth)
        console.log('slider_wrap = ', slider_wrap.clientWidth)
    
        if((slider.clientWidth - margin_left + 12) > slider_wrap.clientWidth) {
            margin_left += 60
            return
        } 
        slider_wrap.style.marginLeft = margin_left + 'px'
        console.log('next', slider.clientWidth - margin_left + 12)
    })

    prev.addEventListener('click', () => {
        if(margin_left == 0) return
        margin_left += 60
        slider_wrap.style.marginLeft = margin_left + 'px'
        console.log('next', margin_left)
    })
}