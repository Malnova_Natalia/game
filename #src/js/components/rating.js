const rating = () => {
  

    data.rating.sort((a, b) => parseFloat(b.points) - parseFloat(a.points));

    let div = document.createElement('div');
    div.classList.add("raiting");
    let content = '';

    for(let i = 0; i < data.rating.length; i++) {
        content += `
        <div class="raiting_item">
            <div class="place">${i + 1}
                <div class="rating_photo">
                    <img src="img/${data.rating[i].img}" alt="">
                </div>
            </div>`

            if(data.friends.filter(x => x.id === data.rating[i].id).length != 0) {
                content += `<p class="fio fio_active">${data.rating[i].name} ${data.rating[i].lastName}</p>`
            } else {
                content += `<p class="fio">${data.rating[i].name} ${data.rating[i].lastName}</p>`
            }
            content += `<p class="expert">${data.rating[i].points}</p>
                </div>
            `;
    }

        div.innerHTML = content;
        document.querySelector('.modal_content').appendChild(div);

}